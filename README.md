# asterisk NAT64 patches

(WIP, more documentation coming soon)

- Place `res_pjsip_nat64.c` in the `res/` directory of the Asterisk sources
  (tested with Asterisk 20.2 but probably works with other older/newer versions?)
- Read the instructions at the top of the file and put in your external IPv4 address
  (something like [this converter](https://www.vultr.com/resources/ipv4-converter/) might help)
- Compile and install Asterisk, making sure the module is loaded (put it in `modules.conf`).
- Run at verbosity level 4 or above (`-vvvv`) if you want the module to print messages when it does things
  - ...and try debug level 4 (`-ddd`) if you really want in-depth spam
- Profit!

## Known bugs, etc

- Your NAT64 prefix has to be the Well-Known Prefix (`64:ff9b::/96`) for now; edit the source if you want to change this

## License

GPLv2 (mainly because that's what Asterisk uses)

