/*
 * res_pjsip_nat64.c -- A very hacky NAT64 translation module for Asterisk.
 *
 * NOTE: You must fill out EXTERNAL_IPV4 below for this to work.
 *       Convert your v4 address from dotted-decimal to decimal, and use that.
 *
 * Copyright (C) 2013, Digium, Inc.
 * Copyright (C) 2023, eta <https://eta.st/> 
 *
 * See http://www.asterisk.org for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*** MODULEINFO
	<depend>pjproject</depend>
	<depend>res_pjsip</depend>
	<depend>res_pjsip_session</depend>
	<support_level>core</support_level>
 ***/

#include "asterisk.h"

#include <pjsip.h>
#include <pjsip_ua.h>
#include <stdbool.h>

#include "asterisk/res_pjsip.h"
#include "asterisk/res_pjsip_session.h"
#include "asterisk/module.h"
#include "asterisk/acl.h"

// NOTE: Fill this out with the external IPv4 address of your NAT64'd host.
static const uint32_t EXTERNAL_IPV4 = 0;

#define is_sip_uri(uri) \
	(PJSIP_URI_SCHEME_IS_SIP(uri) || PJSIP_URI_SCHEME_IS_SIPS(uri))

static const pj_str_t PJ_STR_IP4 = { "IP4", 3 };
static const pj_str_t PJ_STR_IP6 = { "IP6", 3 };

static char *do_nat64_swizzle(char *host_in, bool incoming) {
	ast_debug(4, "Swizzling %s...", host_in);

	struct ast_sockaddr addr_in = { 0 };
	struct ast_sockaddr addr_out = { 0 };

	if (!ast_sockaddr_parse(&addr_in, host_in, PARSE_PORT_FORBID)) {
		ast_debug(4, "failed to parse as IP literal");
		return NULL;
	}
	if (ast_sockaddr_is_ipv4(&addr_in) && incoming) {
		ast_debug(4, "Translating incoming IPv4 to NAT64 IPv6");
		uint32_t v4_in = ast_sockaddr_ipv4(&addr_in);
		struct sockaddr_in6 v6_out = { 0 };
		v6_out.sin6_family = AF_INET6;
		V6_WORD(&v6_out, 0) = htonl(0x0064ff9b);
		V6_WORD(&v6_out, 3) = htonl(v4_in);
		ast_sockaddr_from_sockaddr(&addr_out, (struct sockaddr *) &v6_out);
	}
	if (ast_sockaddr_is_ipv6(&addr_in) && !incoming) {
		struct sockaddr_in6 *v6_in = (struct sockaddr_in6 *) &addr_in.ss;
		// forgive me, for I have sinned
		if (V6_WORD(v6_in, 0) == htonl(0x0064ff9b) && V6_WORD(v6_in, 1) == 0 && V6_WORD(v6_in, 2) == 0) {
			ast_debug(4, "Translating NAT64 IPv6 back into IPv4");
			struct sockaddr_in v4_out = { 0 };
			v4_out.sin_family = AF_INET;
			v4_out.sin_addr.s_addr = V6_WORD(v6_in, 3);
			ast_sockaddr_from_sockaddr(&addr_out, (struct sockaddr *) &v4_out);
		}
		else {
			// FIXME: This should ideally verify whether it's the host IPv6 first.
			// That said, it's not as if we could translate it to anything else...
			ast_debug(4, "Translating non-NAT64 v6 address to the external address");
			struct sockaddr_in v4_out = { 0 };
			v4_out.sin_family = AF_INET;
			v4_out.sin_addr.s_addr = htonl(EXTERNAL_IPV4);
			ast_sockaddr_from_sockaddr(&addr_out, (struct sockaddr *) &v4_out);
		}
	}

	if (!ast_sockaddr_isnull(&addr_out)) {
		char *fmt = ast_sockaddr_stringify_fmt(&addr_out, AST_SOCKADDR_STR_ADDR);
		char *ret = ast_strdup(fmt);
		ast_debug(4, "Successfully swizzled '%s' into '%s'.", host_in, ret);
		return ret;
	}
	else {
		return NULL;
	}

}

static bool pj_sockaddr_is_nat64(pj_sockaddr *addr) {
	char hoststr[PJ_INET6_ADDRSTRLEN];

	pj_sockaddr_print(addr, hoststr, sizeof(hoststr), 0);
	ast_debug(4, "determining whether to swizzle packet for %s", hoststr);
	if (addr->addr.sa_family != PJ_AF_INET6) {
		return false;
	}
	pj_sockaddr_in6 *addr_v6 = (pj_sockaddr_in6 *) addr;
	// This isn't how this macro is meant to be used, but the layout of the pj_sockaddr_in6 is close enough.
	if (V6_WORD(addr_v6, 0) == htonl(0x0064ff9b) && V6_WORD(addr_v6, 1) == 0 && V6_WORD(addr_v6, 2) == 0) {
		ast_debug(4, "looks NAT64-ey to me");
		return true;
	}
	else {
		return false;
	}
}

static bool swizzle_pjsip_uri(pjsip_uri *uri, pjsip_uri_context_e ctxt, pj_pool_t *pool, char *context_str, bool incoming) {
	char fmt[PJSIP_MAX_URL_SIZE];
	pjsip_uri_print(ctxt, uri, fmt, PJSIP_MAX_URL_SIZE);

	if (!is_sip_uri(uri)) {
		ast_debug(4, "ignoring non sip URI '%s'", fmt);
		return false;
	}

	pjsip_sip_uri *sip_uri = (pjsip_sip_uri *) uri;
	char uri_str[PJSIP_MAX_URL_SIZE+1];

	ast_copy_pj_str(uri_str, &sip_uri->host, PJSIP_MAX_URL_SIZE+1);
	char *new_uri_str = do_nat64_swizzle(uri_str, incoming);
	if (new_uri_str) {
		char new_fmt[PJSIP_MAX_URL_SIZE];
		// transmute between the different allocation sources, lol
		pj_str_t new_host_blegh = pj_str(new_uri_str);
		pj_str_t new_host;
	        pj_strdup(pool, &new_host, &new_host_blegh);
		ast_free(new_uri_str);
		pj_strassign(&sip_uri->host, &new_host);
		pjsip_uri_print(ctxt, uri, new_fmt, PJSIP_MAX_URL_SIZE);
		char *incoming_str = incoming ? "incoming" : "outgoing";
		ast_debug(3, "Translated URI '%s' -> '%s' in %s %s", fmt, new_fmt, incoming_str, context_str);
		return true;
	}
	return false;
}

static bool swizzle_sdp_address(pj_str_t *addr, pj_str_t *addr_type, pj_pool_t *pool, char *context_str, bool incoming) {
	char *addr_str = alloca(addr->slen + 1); // null separator!
	ast_copy_pj_str(addr_str, addr, addr->slen + 1);
	char *addr_type_str = alloca(addr_type->slen + 1);
	ast_copy_pj_str(addr_type_str, addr_type, addr_type->slen + 1);

	char *new_addr_str = do_nat64_swizzle(addr_str, incoming);
	if (new_addr_str) {
		// transmute between the different allocation sources, lol
		pj_str_t new_host_blegh = pj_str(new_addr_str);
		pj_str_t new_host;
	        pj_strdup(pool, &new_host, &new_host_blegh);

		// swap out the address
		pj_strassign(addr, &new_host);

		// HACK: reparse (!) and check the address family, since it sometimes doesn't match up
		// with the actual address (what?)
		struct ast_sockaddr new_addr_sa = { 0 };
		if (!ast_sockaddr_parse(&new_addr_sa, new_addr_str, PARSE_PORT_FORBID)) {
			ast_log(LOG_WARNING, "Failed to parse translated SDP address %s\n", new_addr_str);
			return false;
		}

		// swap round the address family
		const pj_str_t *new_family_const = NULL;
		const char *new_family_str = "??";
		if (ast_sockaddr_is_ipv6(&new_addr_sa)) {
			new_family_const = &PJ_STR_IP6;
			new_family_str = "IP6";

			if (!incoming) {
				ast_log(LOG_WARNING, "About to send a NAT64 target an IPv6 SDP address %s!\n", new_addr_str);
			}
		}
		else if (ast_sockaddr_is_ipv4(&new_addr_sa)) {
			new_family_const = &PJ_STR_IP4;
			new_family_str = "IP4";
		}

		if (new_family_const) {
			pj_str_t new_family;
			pj_strdup(pool, &new_family, new_family_const);
			pj_strassign(addr_type, &new_family);
		}

		char *incoming_str = incoming ? "incoming" : "outgoing";
		ast_debug(3, "Translated host '%s' (%s) -> '%s' (%s) in %s %s", addr_str, addr_type_str, new_addr_str, new_family_str, incoming_str, context_str);
		ast_free(new_addr_str);
		return true;
	}
	return false;
}

static bool swizzle_pjsip_host_port(pjsip_host_port *hp, pj_pool_t *pool, char *context_str, bool incoming) {
	char uri_str[PJSIP_MAX_URL_SIZE+1];

	ast_copy_pj_str(uri_str, &hp->host, PJSIP_MAX_URL_SIZE+1);
	char *new_uri_str = do_nat64_swizzle(uri_str, incoming);
	if (new_uri_str) {
		// transmute between the different allocation sources, lol
		pj_str_t new_host_blegh = pj_str(new_uri_str);
		pj_str_t new_host;
	        pj_strdup(pool, &new_host, &new_host_blegh);
		pj_strassign(&hp->host, &new_host);

		char *incoming_str = incoming ? "incoming" : "outgoing";
		ast_debug(3, "Translated host '%s' -> '%s' in %s %s", uri_str, new_uri_str, incoming_str, context_str);
		ast_free(new_uri_str);
		return true;
	}
	return false;
}

static void swizzle_message(pj_pool_t *pool, pjsip_msg *msg, struct pjmedia_sdp_session *sdp, bool incoming) {
	int n_headers = 0;
	int n_sdp = 0;

	if (msg->type == PJSIP_REQUEST_MSG) {
		pjsip_uri *uri = pjsip_uri_get_uri(msg->line.req.uri);
		n_headers += swizzle_pjsip_uri(uri, PJSIP_URI_IN_REQ_URI, pool, "request line", incoming);
	}

        pjsip_contact_hdr *contact = pjsip_msg_find_hdr(msg, PJSIP_H_CONTACT, NULL);
        if (contact && !contact->star) {
                pjsip_uri *uri = pjsip_uri_get_uri(contact->uri);
		n_headers += swizzle_pjsip_uri(uri, PJSIP_URI_IN_CONTACT_HDR, pool, "Contact header", incoming);
	}

	pjsip_fromto_hdr *to = pjsip_msg_find_hdr(msg, PJSIP_H_TO, NULL);
	if (to) {
                pjsip_uri *uri = pjsip_uri_get_uri(to->uri);
		n_headers += swizzle_pjsip_uri(uri, PJSIP_URI_IN_CONTACT_HDR, pool, "To header", incoming);
	}

	pjsip_fromto_hdr *from = pjsip_msg_find_hdr(msg, PJSIP_H_FROM, NULL);
	if (from) {
                pjsip_uri *uri = pjsip_uri_get_uri(from->uri);
		n_headers += swizzle_pjsip_uri(uri, PJSIP_URI_IN_CONTACT_HDR, pool, "From header", incoming);
	}

        pjsip_via_hdr *via = pjsip_msg_find_hdr(msg, PJSIP_H_VIA, NULL);
	if (via) {
		n_headers += swizzle_pjsip_host_port(&via->sent_by, pool, "Via header", incoming);
		//via = pjsip_msg_find_hdr(msg, PJSIP_H_VIA, via);
	}

	if (sdp) {
		n_sdp += swizzle_sdp_address(&sdp->origin.addr, &sdp->origin.addr_type, pool, "SDP origin", incoming);
		n_sdp += swizzle_sdp_address(&sdp->conn->addr, &sdp->conn->addr_type, pool, "SDP c= line", incoming);

		for (int stream = 0; stream < sdp->media_count; ++stream) {
			if (sdp->media[stream]->conn) {
				pj_str_t *addr = &sdp->media[stream]->conn->addr;
				pj_str_t *addr_type = &sdp->media[stream]->conn->addr_type;

				n_sdp += swizzle_sdp_address(addr, addr_type, pool, "SDP media line", incoming);
			}
		}
	}

	int swizzled = n_headers + n_sdp;

	if (swizzled > 0) {
		char *incoming_str = incoming ? "incoming" : "outgoing";
		ast_verb(4, "NAT64 %s translation mangled %d addresses (%d headers, %d sdp)", incoming_str, swizzled, n_headers, n_sdp);
	}
}

static pj_status_t nat64_on_tx_message(pjsip_tx_data *tdata) {
	pj_status_t rc = PJ_SUCCESS;
	pj_pool_t *pool = tdata->pool;
	pjsip_msg *msg = tdata->msg;

	// Only do translation if the packet is going via a NAT64 gateway.
	// Otherwise, we'll break communication with native v6 hosts!
	if (pj_sockaddr_is_nat64(&tdata->tp_info.dst_addr)) {
		struct pjmedia_sdp_session *sdp = NULL;

		// Check to see if there's SDP data we need to rewrite.
		pjsip_sdp_info *sdp_info = pjsip_tdata_get_sdp_info(tdata);
		if (sdp_info && sdp_info->sdp_err == PJ_SUCCESS && sdp_info->sdp && sdp_info->sdp->conn) {
			sdp = sdp_info->sdp;
		}

		swizzle_message(pool, msg, sdp, false);

		pjsip_tx_data_invalidate_msg(tdata);
	}

	return rc;
}

static pj_bool_t nat64_on_rx_message(pjsip_rx_data *rdata)
{
	pj_bool_t res = PJ_FALSE;
	pjsip_msg *msg = rdata->msg_info.msg;
	pj_pool_t *pool = rdata->tp_info.pool;
	
	// Only do translation if the packet is coming from a NAT64 gateway.
	// Otherwise, we'll break communication with native v6 hosts!
	if (pj_sockaddr_is_nat64(&rdata->pkt_info.src_addr)) {
		struct pjmedia_sdp_session *sdp = NULL;

		// Check to see if there's SDP data we need to rewrite.
		pjsip_sdp_info *sdp_info = pjsip_rdata_get_sdp_info(rdata);
		if (sdp_info && sdp_info->sdp_err == PJ_SUCCESS && sdp_info->sdp && sdp_info->sdp->conn) {
			sdp = sdp_info->sdp;
		}

		swizzle_message(pool, msg, sdp, true);
	}

	return res;
}


static pjsip_module nat64_module = {
	.name = { "NAT64", 5 },
	.id = -1,
	.priority = PJSIP_MOD_PRIORITY_TSX_LAYER - 3,
	.on_rx_request = nat64_on_rx_message,
	.on_rx_response = nat64_on_rx_message,
	.on_tx_request = nat64_on_tx_message,
	.on_tx_response = nat64_on_tx_message,
};


static int unload_module(void)
{
	ast_sip_unregister_service(&nat64_module);
	return 0;
}

static int load_module(void)
{
	if (EXTERNAL_IPV4 == 0) {
		ast_log(LOG_ERROR, "You must set EXTERNAL_IPV4 and recompile in res_pjsip_nat64.c\n");
		return AST_MODULE_LOAD_DECLINE;
	}
	if (ast_sip_register_service(&nat64_module)) {
		ast_log(LOG_ERROR, "Could not register NAT64 service!\n");
		return AST_MODULE_LOAD_DECLINE;
	}

	return AST_MODULE_LOAD_SUCCESS;
}

AST_MODULE_INFO(ASTERISK_GPL_KEY, AST_MODFLAG_LOAD_ORDER, "PJSIP NAT64 translation",
	.support_level = AST_MODULE_SUPPORT_CORE,
	.load = load_module,
	.unload = unload_module,
	.load_pri = AST_MODPRI_APP_DEPEND,
	.requires = "res_pjsip,res_pjsip_session",
);
